import {
    ESPLoader,
    IEspLoaderTerminal,
    FlashOptions,
    LoaderOptions,
    Transport,
} from "esptool-js";
type espTerminal = IEspLoaderTerminal;
import {store} from 'src/store';
import {getSelectedConnectedDevice} from 'src/store/devicesSlice';


let terminal: espTerminal;

let device: any = null;
let transport: Transport;
let chip: string;
let esploader: ESPLoader;
let manifest: Manifest;
let files: BuildFiles[];

const default_manifest = "https://gitlab.com/api/v4/projects/38750249/jobs/artifacts/master/raw/firmware/manifest.json\?job\=build-job"

const default_loaderOptions = {
    baudrate: 460800,
    // debugLogging: true,
} as LoaderOptions;

const default_flashOptions = {
    flashSize: "keep",
    flashFreq: "80MHz",
    flashMode: "dio",
    compress: true,
} as FlashOptions;


interface Manifest {
    name: string,
    version: string,
    builds: BuildManifest[],
}

interface BuildManifest {
  chipFamily: "ESP32" | "ESP8266" | "ESP32-S2" | "ESP32-S3" | "ESP32-C3";
  parts: {
    path: string;
    offset: number;
  }[];
}

interface BuildFiles {
    data: string,
    address: number,
}

interface AppDescription {
    version: string,
    project_name: string,
    compile_time: string,
    compile_date: string,
    idf_version: string,
    app_elf_sha256: string,
}



export const isSupported = () => {
    return ('serial' in navigator);
}

export const isConnected = () => {
    return (device !== null);
}


export const espConnect = async (espTerminal: espTerminal) => {

    terminal = espTerminal;

    if (!isSupported()) {
        console.warn("WebSerial is not supported in this browser");
        return;
    }
    // switch HID device to bootloader
    const selectedDevice = getSelectedConnectedDevice(store.getState());
    console.log(selectedDevice);
    if (selectedDevice) {
        console.log("Jumping device to bootloader")
        terminal.writeLine("Jumping to bootloader");
        terminal.writeLine("Warning: you might need to click on Connect again");
        await selectedDevice.api.jumpToBootloader();
    }

    console.log("Connecting");

    if (device === null) {
        device = await navigator.serial.requestPort();
        transport = new Transport(device);
    }

    try {
        const flashOptions = {
            ...default_loaderOptions,
            transport,
            terminal: terminal,
        } as LoaderOptions;
        esploader = new ESPLoader(flashOptions);

        chip = await esploader.main_fn();
    }
    catch (e) {
        console.error(e);
        terminal.writeLine(`Error: ${e.message}`);
        await espDisconnect();
        return;
    }
    console.log("Settings done for: " + chip);

    loadManifest(default_manifest);

}


const toBinString = (bytes: Uint8Array) =>
  bytes.reduce((str, byte) => str + byte.toString(2).padStart(8, '0'), '');


const loadManifest = async (manifest_url: string) => {

    console.log("Loading FW manifest file");
    terminal.writeLine("Loading FW manifest file...");

    const manifestUrl = new URL(manifest_url, location.toString());
    manifest = await fetch(manifestUrl.toString(), {cache: "no-cache"})
        .then((response) => {
            if (!response.ok) {
                throw new Error("Downloading FW manifest " + manifestUrl);
            }
            return response.json();
        })
        .then((data) => {
            return data;
        })
        .catch((err) => {
            console.error("Error: ", err);
            terminal.writeLine(`Error: ${err.message}`);
            return;
        });

    if (!manifest) {
        return;
    }
    console.info("Loaded manifest", manifest);
    terminal.writeLine("Loaded");
    terminal.writeLine("Manifest version: " + manifest.version);
    

    var build: BuildManifest | undefined;
    build = manifest.builds.find((b) => b.chipFamily === chip);
    if (build === undefined) {
        console.error("Chip family not found in manifest");
        return;
    }
    console.info("Selected build", build);

    files = [];
    let totalSize = 0;

    terminal.writeLine("Loading FW files...");
    const fileBuffers = build.parts.map(async (part) => {
        const url = new URL(part.path, manifestUrl);
        manifestUrl.searchParams.forEach((value, key) => { // add same query params as manifest url
            url.searchParams.set(key, value);
        })
        const buffer = await fetch(url.toString(), {cache: "no-cache"})
            .then((response) => {
                if (!response.ok) {
                    throw new Error("Downloading FW file: " + part.path);
                }
                // return response.arrayBuffer();
                return response.blob();
            })
            .then((buffer) => {
                return buffer;
            })
            .catch((err) => {
                console.error("Error: ", err);
                terminal.writeLine(`Error: ${err.message}`);
            });

        const reader = new FileReader();

        if (buffer) {
            return new Promise<string>((resolve) => {
                reader.addEventListener("load", () => resolve(reader.result as string));
                reader.readAsBinaryString(buffer);
            });
        }
        return null;

    });

    for (let part = 0; part < fileBuffers.length; part++) {
        const data = await fileBuffers[part];
        if (data !== null) {
            files.push({ data: data, address: build.parts[part].offset });
            totalSize += data.length;
        }
        else {
            return;
        }
    }
    terminal.writeLine("Loaded");

    terminal.writeLine("Manifest app binary info:");
    files.forEach((file) => {
        parseAppDescription(file.data);
    })

    const flash = await espReadAppDescription();
    if (flash) {
        terminal.writeLine("Device current app binary info:");
        parseAppDescription(flash);
    }

}


const APP_PARTITION_OFFSET = 0x10000;
const DROM_OFFSET = 0x18;
const APP_DESC_OFFSET = 0x08;
const APP_DESC_LENGTH = 272;
const ESP_APP_DESC_MAGIC_WORD = 0xABCD5432; // in hex big-endian
const ESP_APP_DESC_MAGIC_WORD_STR = "2TÍ«"; // in ascii little-endian


const espReadAppDescription = async () => {

    const address = APP_PARTITION_OFFSET + DROM_OFFSET + APP_DESC_OFFSET;

    console.log("Reading app info from device at address: 0x" + address.toString(16));
    const binary = await esploader.read_flash(address, APP_DESC_LENGTH)
        .then((array) => {
            return array;
        })
        .catch((err) => {
            console.error("Error: ", err);
            terminal.writeLine(`Error: ${err.message}`);
        })

    if (binary) {
        const reader = new FileReader();
        const flash_string = await new Promise<string>((resolve) => {
            reader.addEventListener("load", () => resolve(reader.result as string));
            reader.readAsBinaryString(new Blob([binary]));
        });
        return flash_string;
    }
    return null;
}


const parseAppDescription = (data: string) => {

    const idx = data.indexOf(ESP_APP_DESC_MAGIC_WORD_STR);
    if (idx < 0) {
        console.warn("Unable to read magic number");
        return;
    }

    interface AppDescriptionMember {
        [key: string]: {
            offset: number,
            length: number,
        }
    }

    const app_desc_members: AppDescriptionMember = {
        version:        { offset: 16,  length: 32 },
        project_name:   { offset: 48,  length: 32 },
        compile_time:   { offset: 80,  length: 16 },
        compile_date:   { offset: 96,  length: 16 },
        idf_version:    { offset: 112, length: 32 },
        app_elf_sha256: { offset: 144, length: 32 },
    }

    const getMember = (data: string, member: string, trim: boolean = true) => {
        let str = "";
        if (member in app_desc_members) {
            str = data.slice(
                idx + app_desc_members[member].offset, 
                idx + app_desc_members[member].offset + app_desc_members[member].length
            )
            if (trim) {
                str = str.replace(/[\x00]/gu, ""); // remove 0x00 char values
            }
        }
        return str;
    }

    const app_desc: AppDescription = {
        version:        getMember(data, 'version'),
        project_name:   getMember(data, 'project_name'),
        compile_time:   getMember(data, 'compile_time'),
        compile_date:   getMember(data, 'compile_date'),
        idf_version:    getMember(data, 'idf_version'),
        app_elf_sha256: getMember(data, 'app_elf_sha256', false),
    }
    console.log(app_desc)

    terminal.writeLine("\tVersion: " + app_desc.version);
    terminal.writeLine("\tCompile time: " + app_desc.compile_date + " " + app_desc.compile_time);
    terminal.writeLine("\tIDF Version: " + app_desc.idf_version);
    // terminal.writeLine("\tELF file SHA256: " + app_desc.app_elf_sha256);

    return app_desc;
}


export const espFlash = async (terminal: any, erase_before: boolean = false, reset_after: boolean = true) => {

    if (device === null || esploader === null) {
        const msg = "No device connected";
        console.error(msg);
        terminal.writeLine(`Error: ${msg}`);
        return;
    } 
    if (!manifest || !files) {
        const msg = 'Unable to flash, missing manifest or build files';
        console.error(msg);
        terminal.writeLine(`Error: ${msg}`);
        return;
    }

    try {
        const flashOptions: FlashOptions = {
            ...default_flashOptions,
            fileArray: files,
            eraseAll: erase_before,
            // reportProgress: (fileIndex, written, total) => {
            //     // progressBars[fileIndex].value = (written / total) * 100;
            //     console.log("Writing %" + (written / total) * 100);
            // },
            // calculateMD5Hash: (image) => CryptoJS.MD5(CryptoJS.enc.Latin1.parse(image)),
        } as FlashOptions;
        console.log("Flashing");
        await esploader.write_flash(flashOptions);
        console.log("Done flashing");
        terminal.writeLine("Hard resetting via RTS pin...");
        console.log("Resetting");
        await esploader.hard_reset();
        console.log("Done resetting");
    }
    catch (e) {
        console.error(e);
        terminal.writeLine(`Error: ${e.message}`);
        await espDisconnect();
        return;
    }

}


export const espReset = async (terminal: any) => {

    if (device === null) {
        device = await navigator.serial.requestPort();
        transport = new Transport(device);
    }

    console.log("Resetting device via DTR pin");
    terminal.writeLine("Resetting...");

    await transport.setDTR(false);
    await new Promise((resolve) => setTimeout(resolve, 100));
    await transport.setDTR(true);

}


export const espDisconnect = async () => {

    console.log("Disconnecting device");

    if (device !== null) {
        await transport.disconnect();
    }
    device = null;
    chip = "";

}



