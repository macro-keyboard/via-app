import '@webscopeio/react-textarea-autocomplete/style.css';
import React from 'react';
import {render} from 'react-dom';
import Root from './containers/Root';

render(<Root />, document.getElementById('root'));
