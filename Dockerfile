ARG NODE_VERSION=18.12.1
ARG NGINX_VERSION=1.23.2-alpine

FROM --platform=$BUILDPLATFORM node:${NODE_VERSION} as builder

# set working directory
WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH
# install app dependencies
COPY package.json ./
COPY yarn.lock ./
RUN yarn install

COPY . ./
RUN yarn build


FROM nginx:${NGINX_VERSION}

RUN addgroup -S appuser && \
    adduser -S -D -h /app -u 9999 -G appuser appuser
# Copy local config file
COPY nginx.conf /etc/nginx/nginx.conf

WORKDIR /app
# Remove default nginx static assets
RUN rm -rf ./*

# add static app
COPY --from=builder /app/build ./

RUN chown appuser:appuser -R /app && \
    chown -R appuser:appuser /var/cache/nginx && \
    chown -R appuser:appuser /var/log/nginx && \
    chown -R appuser:appuser /etc/nginx/conf.d
RUN touch /var/run/nginx.pid && \
    chown -R appuser:appuser /var/run/nginx.pid

USER appuser

EXPOSE 3000
ENTRYPOINT ["nginx", "-g", "daemon off;"]