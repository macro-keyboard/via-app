# usevia.app

<img src="public/images/chippy.png" width="300"/>

## Useful commands
### yarn start

Runs the app in the development mode.
Open http://localhost:8080 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### yarn build

Builds a static copy of your site to the `build/` folder.
Your app is ready to be deployed!

### npm test

Launches the application test runner.
Run with the `--watch` flag (`npm test -- --watch`) to run in interactive watch mode.

### docker

Added the ability to run the app in a docker container using Nginx server.

#### To build

```shell
docker build -t via-app .
```

- Multi-arch

```shell
docker buildx build --push \
	 --platform linux/amd64,linux/arm64,linux/arm/v7 \
    -t registry.gitlab.com/macro-keyboard/via-app:latest \
    -t registry.gitlab.com/macro-keyboard/via-app:$(git rev-parse --short HEAD) .
```

#### To run

```shell
docker run --rm -it -p 8080:3000 via-app
```
