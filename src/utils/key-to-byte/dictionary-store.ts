import basicKeyToByte from './default';
import v10BasicKeyToByte from './v10';
import v11BasicKeyToByte from './v11';
import v12BasicKeyToByte from './v12';
import v11CustomBasicKeyToByte from './v11custom';

export function getBasicKeyDict(version: number) {
  switch (version) {
    // Our custom version are defined as v1.11 (0x010B)
    // MSB defines major version number
    case (0x100 + 12):
    case (0x100 + 11): {
      return v11CustomBasicKeyToByte;
    }

    // Normal VIA versions
    case 13:
    case 12: {
      return v12BasicKeyToByte;
    }
    case 11: {
      return v11BasicKeyToByte;
    }
    case 10: {
      return v10BasicKeyToByte;
    }
    default: {
      return basicKeyToByte;
    }
  }
}
