import React from 'react';
import {Pane} from './pane';
import styled from 'styled-components';
import {ControlRow, Label, Detail, OverflowCell } from './grid';
import {AccentSlider} from '../inputs/accent-slider';
import {AccentButton} from '../inputs/accent-button';
import {ErrorMessage} from '../styled';
import {useDispatch} from 'react-redux';
import {useAppSelector} from 'src/store/hooks';
import {
  getAllowKeyboardKeyRemapping,
  getShowDesignTab,
  getDisableFastRemap,
  getDisableHardwareAcceleration,
  getRestartRequired,
  getShowUpdateTab,
  toggleCreatorMode,
  toggleFastRemap,
  toggleHardwareAcceleration,
  requireRestart,
  toggleKeyRemappingViaKeyboard,
  toggleUpdaterMode,
} from 'src/store/settingsSlice';
import {isSupported, espDisconnect} from 'src/utils/esp-web';


const RestartMessage = styled(ErrorMessage)`
  margin: 0;
  font-size: 20px;
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 0 12px;
`;

const DebugPane = styled(Pane)`
  display: grid;
  max-width: 100vw;
  grid-template-columns: 100vw;
  grid-template-rows: min-content;
  background: var(--color_light-jet);
`;

export const Settings = () => {
  const dispatch = useDispatch();
  const showDesignTab = useAppSelector(getShowDesignTab);
  const disableFastRemap = useAppSelector(getDisableFastRemap);
  const showUpdateTab = useAppSelector(getShowUpdateTab);

  const onUpdaterToggle = (state: boolean) => {
    dispatch(toggleUpdaterMode());
    if (!state) {
      espDisconnect();
    }
  }

  return (
    <DebugPane>
      <OverflowCell>
        <Container>
          <ControlRow>
            <Label>Show Design tab</Label>
            <Detail>
              <AccentSlider
                onChange={() => dispatch(toggleCreatorMode())}
                isChecked={showDesignTab}
              />
            </Detail>
          </ControlRow>
          <ControlRow>
            <Label>Fast Key Mapping</Label>
            <Detail>
              <AccentSlider
                onChange={() => dispatch(toggleFastRemap())}
                isChecked={!disableFastRemap}
              />
            </Detail>
          </ControlRow>
          <ControlRow>
            <Label>Show Update tab</Label>
            <Detail>
              <AccentSlider
                onChange={(checked) => onUpdaterToggle(checked)}
                isChecked={showUpdateTab}
                isDisabled={!isSupported()}
              />
            </Detail>
          </ControlRow>
        </Container>
      </OverflowCell>
    </DebugPane>
  );
};
