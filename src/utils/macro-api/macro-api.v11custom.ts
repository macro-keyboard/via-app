import {isAutocompleteKeycode} from '../autocomplete-keycodes';
import type {KeyboardAPI} from '../keyboard-api';
import type { ValidationResult, IMacroAPI} from './macro-api.common';

import {getByteForCode, getCodeForByte} from '../key';

// copied from keyboard-api.ts
const shiftTo16Bit = ([hi, lo]: [number, number]): number => (hi << 8) | lo;

const shiftFrom16Bit = (value: number): [number, number] => [
  value >> 8,
  value & 255,
];


// Only comma-separated valid keycodes should be allowed in unescaped action blocks: {KC_VALID_KEYCODE, KC_ANOTHER_ONE}
// Empty action blocks can't be persisted, so should fail: {}
export function validateMacroExpressionV11Custom(
  expression: string,
): ValidationResult {
  let unclosedBlockRegex, keycodeBlockRegex;

  // Eval the macro regexes to prevent script errors in browsers that don't
  // have support for the negative lookbehind feature.
  // See: https://caniuse.com/js-regexp-lookbehind
  try {
    unclosedBlockRegex = eval('/(?<!\\\\){(?![^{]*})/');
    keycodeBlockRegex = eval('/(?<!\\\\){(.*?)}/g');
  } catch (e) {
    // TODO: Display a message to the user
    console.error('Lookbehind support is not supported in this browser.');
  }

  // Check for unclosed action blocks
  if (expression.match(unclosedBlockRegex)) {
    return {
      isValid: false,
      errorMessage:
        "Looks like a keycode block - {} - is unclosed! Are you missing an '}'?",
    };
  }

  // Validate each block of keyactions
  let groups: RegExpExecArray | null = null;
  while ((groups = keycodeBlockRegex.exec(expression))) {
    const csv = groups[1].replace(/\s+/g, ''); // Remove spaces

    // Empty action blocks {} can't be persisted
    if (!csv.length) {
      return {
        isValid: false,
        errorMessage:
          "Sorry, I can't handle empty {}. Fill them up with keycodes or use \\{} to tell the macro to literally type {}",
      };
    }

    // Test if it's a delay expression
    if (/\d+/.test(csv)) {
      if (/\d{5,}/.test(csv)) {
        return {
          isValid: false,
          errorMessage: `Invalid delay: ${csv}. Please use a delay value of 9999 or less.`,
        };
      }
    } 
    // else {
    //   // Otherwise test for keycode expressions
    //   const invalidKeycodes = csv
    //     .split(',')
    //     .filter(
    //       (keycode) => keycode.trim().length && !keycodeInMaster(keycode, )
    //     );
    //     console.log(csv, invalidKeycodes);
    //   if (invalidKeycodes.length) {
    //     return {
    //       isValid: false,
    //       errorMessage: `Whoops! Invalid keycodes detected inside {}: ${invalidKeycodes.join(
    //         ', ',
    //       )}`,
    //     };
    //   }
    // }
  }

  return {
    isValid: true,
    errorMessage: undefined,
  };
}

export class MacroAPIV11Custom implements IMacroAPI {
  constructor(
    private keyboardApi: KeyboardAPI,
    private basicKeyToByte: Record<string, number>,
    private byteToKey: Record<number, string>,
  ) {}

  async readMacroExpressions(): Promise<string[]> {
    const bytes = await this.keyboardApi.getMacroBytes();
    const macroCount = await this.keyboardApi.getMacroCount();
    const macroSize = await this.keyboardApi.getMacroBufferSize();
    const macroLength = macroSize / macroCount / 2;
    // console.log("Macro bytes: ", bytes);
    // console.log("Macro count: ", macroCount, " size: ", macroSize, "-> length: ", macroLength);

    let macroId = 0;
    let i = 0;
    let j = 0;
    const expressions: string[] = [];
    let currentExpression = [];
    let currentChord = [];

    // If macroCount is 0, macros are disabled
    if (macroCount === 0) {
      throw Error('Macros are disabled');
    }

    while (i < bytes.length && macroId < macroCount) {
        j = 0;
        while (j < macroLength) {
            // let keycode = bytes[i];
            let keybyte = shiftTo16Bit([bytes[i], bytes[i+1]]);
            let keycode = getCodeForByte(keybyte, this.basicKeyToByte ,this.byteToKey);
            currentChord.push(keycode);
            j++;
            i += 2;
        }
        expressions[macroId] = `{${currentChord.join(',')}}`;
        currentChord = [];
        macroId++;

    }

    return expressions;
  }

  async writeMacroExpressions(expressions: string[]) {
    const macroBytes = expressions.flatMap((expression) => {
      const validationResult = validateMacroExpressionV11Custom(expression);
      if (!validationResult.isValid) {
        throw validationResult.errorMessage;
      }
      const bytes: number[] = [];
      let i = 0;
      while (i < expression.length) {
        const char = expression[i];
        // Check for keycode block, peek behind to make sure there's no escape char \
        if (char === '{' && expression[i - 1] !== '\\') {
          const keyActionEnd = expression.indexOf('}', i + 1);
          if (keyActionEnd < 0) {
            throw new Error("Syntax error: KeyAction block must end with '}'");
          }
          const block = expression.substr(i + 1, keyActionEnd - i - 1);
        
          const keycodes = block.split(',');
          keycodes.forEach((keycode) => {
            let keybyte = getByteForCode(keycode, this.basicKeyToByte);
            let keybytes = shiftFrom16Bit(keybyte);
            keybytes.forEach((byte) => {
              bytes.push(byte);
            });
          });
           
          i = keyActionEnd; // fastforward cursor to end of action block
        }
        i++;
      }

      return bytes;
    });

    // console.log("writing macro bytes: ", macroBytes);
    await this.keyboardApi.setMacroBytes(macroBytes);
  }
}
