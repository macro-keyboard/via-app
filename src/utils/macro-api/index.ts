import {getByteToKey} from '../key';
import {getBasicKeyDict} from '../key-to-byte/dictionary-store';
import type {KeyboardAPI} from '../keyboard-api';
import {MacroAPI, MacroValidator, validateMacroExpression} from './macro-api';
import {MacroAPIV11, validateMacroExpressionV11} from './macro-api.v11';
import {MacroAPIV11Custom, validateMacroExpressionV11Custom} from './macro-api.v11custom';

export const getMacroAPI = (protocol: number, keyboardApi: KeyboardAPI) => {
  const byteToKey = getByteToKey(getBasicKeyDict(protocol));
  const keyToByte = getBasicKeyDict(protocol);

  switch (protocol) {
    // Our custom version are defined as v1.11 (0x010B)
    // MSB defines major version number
    case (0x100 + 12):
    case (0x100 + 11): {
      return new MacroAPIV11Custom(keyboardApi, keyToByte, byteToKey);
    }

    // Normal VIA versions
    case 13:
    case 12:
    case 11: {
      return new MacroAPIV11(keyboardApi, keyToByte, byteToKey);
    }
    case 10:
    default: {
      return new MacroAPI(keyboardApi, keyToByte, byteToKey);
    }
  }
};

export const getMacroValidator = (protocol: number): MacroValidator => {
  switch (protocol) {
    // Our custom version are defined as v1.11 (0x010B)
    // MSB defines major version number
    case (0x100 + 12):
    case (0x100 + 11): {
      return validateMacroExpressionV11Custom;
    }

    // Normal VIA versions
    case 13:
    case 12:
    case 11: {
      return validateMacroExpressionV11;
    }
    case 10:
    default: {
      return validateMacroExpression;
    }
  }
}
