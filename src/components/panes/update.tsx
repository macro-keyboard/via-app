import React from 'react';
import {Pane} from './pane';
import styled from 'styled-components';
import {ControlRow, Label, OverflowCell } from './grid';
import {AccentSlider} from '../inputs/accent-slider';
import {AccentButton} from '../inputs/accent-button';
import {
  getShowUpdateTab,
  toggleUpdaterMode,
} from 'src/store/settingsSlice';
import {
  espConnect,
  espFlash,
  espReset,
  espDisconnect,
} from 'src/utils/esp-web';
import {XTerm} from 'xterm-for-react';


const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  padding: 0 12px;
`;

const UpdatePane = styled(Pane)`
  display: grid;
  max-width: 100vw;
  grid-template-columns: 100vw;
  grid-template-rows: min-content;
  background: var(--color_light-jet);
`;

const ActionButtons = styled(ControlRow)`
  width: 100%;
  max-width: 430px;
  justify-items: space-evenly;
  column-gap: 0.25rem;
  align-items: center;
  padding: 1em 0 0.5rem 0 ;
`;

const FlashOptions = styled(ControlRow)`
  justify-content: space-evenly;
  align-items: center;
`;

const OptionSlider = styled.div`
  display: flex;
  align-items: center;
  column-gap: 1rem;
`;

const Terminal = styled(ControlRow)`
  line-height: 100%;
  justify-content: center;
  padding: 0.5em;
`;

const terminalTheme = {
  background: "#202020",
  foreground: "#757575",
};


export const UpdateTab = () => {
  const xtermRef = React.useRef<XTerm | null>(null);
  const espTerminal = {
    clean() {
      xtermRef.current?.terminal.clear();
    },
    writeLine(data: string) {
      xtermRef.current?.terminal.writeln(data);
    },
    write(data: string) {
      xtermRef.current?.terminal.write(data);
    },
  };
  const [eraseBefore, setEraseBefore] = React.useState(false);
  const [resetAfter,  setResetAfter] = React.useState(true);


  return (
    <UpdatePane>
      <OverflowCell>
        <Container>
          <ActionButtons>
            <AccentButton
               onClick={() => espConnect(espTerminal)}
             >
              Connect
            </AccentButton>
            <AccentButton
               onClick={() => espFlash(espTerminal, eraseBefore, resetAfter)}
             >
              Flash
            </AccentButton>
            <AccentButton
               onClick={() => espReset(espTerminal)}
             >
              Reset
            </AccentButton>
          </ActionButtons>
          <FlashOptions>
            <OptionSlider>
              <Label>Erase before flashing</Label>
              <AccentSlider
                onChange={(checked) => setEraseBefore(checked)}
                isChecked={eraseBefore}
              />
            </OptionSlider>
            <OptionSlider>
              <Label>Reset after flashing</Label>
              <AccentSlider
                onChange={(checked) => setResetAfter(checked)}
                isChecked={resetAfter}
              />
            </OptionSlider>
          </FlashOptions>
        </Container>
        <Container>
          <Terminal>
            <XTerm ref={xtermRef} options={{ theme: terminalTheme }}/>
          </Terminal>
        </Container>
      </OverflowCell>
    </UpdatePane>
  );
};
